//
//  ViewController.swift
//  Series
//
//  Created by Test User on 2019. 09. 14..
//  Copyright © 2019. NJE GAMF. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // from series name generator
    var series = [
        "Bitten Drama",
        "Animal Story",
        "Spring Romance",
        "Cardboard Drama",
        "Demon Mind",
        "Weird Justice",
        "Adorable Smiles",
        "Lunarwitch",
        "Hyperwitch",
        "Scribblekeeper",
        "Rainy Story",
        "Snowy Fantasies",
        "Geeky Drama",
        "Destined Fantasy",
        "Conjured Smiles",
        "Anonymous Outcast",
        "Distant Cloud",
        "Fantasyvoice",
        "Abyssluck",
        "Bitterpride",
        "Sidekick Fantasy",
        "Unlucky Adventures",
        "Secret Legend",
        "Sidekick Adventures",
        "Bitten Ghost",
        "Human Sinner",
        "Paper Sinner",
        "Mortalwisp",
        "Oceanblood",
        "Sweetvision",
    ]
    
    @IBOutlet weak var seriesTableView: UITableView!
    @IBOutlet weak var numberOfItemsLabel: UILabel!
    @IBOutlet weak var seriesNameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberOfItemsLabel.text = "Number of Items: \(series.count)"
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return series.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = series[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(series[indexPath.row]) selected")
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteTitle = NSLocalizedString("Delete", comment: "Delete action")
        let deleteAction = UITableViewRowAction(style: .destructive,
                                                title: deleteTitle) { (action, indexPath) in
                                                    self.series.remove(at: indexPath.row)
                                                    self.refreshUI()
        }
        
        return [deleteAction]
    }
    
    @IBAction func addOnTouch(_ sender: UIButton) {
        series.append(seriesNameTextField.text!)
        refreshUI()
        seriesNameTextField.text = ""
    }
    
    func refreshUI() {
        seriesTableView.reloadData()
        numberOfItemsLabel.text = "Number of Items: \(series.count)"
    }
}

